# sudo apt-get install python-tk
# sudo apt-get install python3.2-tk

import turtle
PADDLE_WIDTH = 5
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


wn = turtle.Screen()
wn.title("Pong")
wn.bgcolor("black")
wn.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
wn.tracer(0)

# Paddle A
PADDLE_A_X = -380
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.color("red")
paddle_a.shapesize(stretch_wid=PADDLE_WIDTH,stretch_len=1)
paddle_a.penup()
paddle_a.goto(PADDLE_A_X, 0)

paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.color("blue")
paddle_b.shapesize(stretch_wid=PADDLE_WIDTH,stretch_len=1)
paddle_b.penup()
paddle_b.goto(350, 0)

ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("pink")
ball.penup()
ball.goto(0, 0)
ball.dx = 0.2
ball.dy = 0.2

# Pen
pen = turtle.Turtle()
pen.speed(0)
pen.shape("square")
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Player A: 0  Player B: 0", align="center", font=("Courier", 24, "normal"))


def set_y(y, paddle):
    max_up = SCREEN_HEIGHT / 2 - (PADDLE_WIDTH / 2) * 20
    max_down = -max_up 
    if y >= max_up:
        paddle.sety(max_up)
    elif y <= max_down:
        paddle.sety(max_down)
    else:
        paddle.sety(y)

def paddle_a_up():
    y = paddle_a.ycor()
    y += 40
    set_y(y, paddle_a)

def paddle_a_down():
    y = paddle_a.ycor()
    y -= 40
    set_y(y, paddle_a)


def paddle_b_up():
    y = paddle_b.ycor()
    y += 40
    set_y(y, paddle_b)

def paddle_b_down():
    y = paddle_b.ycor()
    y -= 40
    set_y(y, paddle_b)

wn.listen()
wn.onkeypress(paddle_a_down, "s")
wn.onkeypress(paddle_a_up, "w")
wn.onkeypress(paddle_b_up, 'Up')
wn.onkeypress(paddle_b_down, 'Down')

score_a = 0
score_b = 0

while True:
    wn.update()
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

    if ball.ycor() > 290:
        ball.dy = -0.1

    if ball.ycor() < -290:
        ball.dy = 0.1

    if ball.xcor() > 390:
        ball.dx = -0.1

    if ball.xcor() < -390:
        ball.dx = 0.1

    if ball.xcor() <= PADDLE_A_X + 10 \
        and ball.ycor() < paddle_a.ycor() + 50 \
        and ball.ycor() > paddle_a.ycor() - 50 \
        and ball.dx < 0:
        ball.dx *= -1
        pen.clear()
        score_a += 1
        pen.write("Player A: {}  Player B: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))

    print(f'y {paddle_a.ycor()}')
    print(f'x {paddle_a.xcor()}')
